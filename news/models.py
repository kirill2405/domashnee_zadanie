from django.db import models

class News(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    pub_date = models.DateTimeField()
    text = models.TextField()
    
    class Meta:
        verbose_name_plural = "News"
        verbose_name = "News123"
# Create your models here.
