from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^news/', include('news.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^contacts/', include('contacts.urls')),
    url(r'^main/', include('main.urls')),
    url(r'^speakers/', include('speakers.urls')),
    url(r'^organizations/', include('organizations.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
]

if settings.DEBUG:
   urlpatterns += patterns('',
   url(r'^404$', TemplateView.as_view(template_name='404.html')),
   url(r'^500$', TemplateView.as_view(template_name='500.html')),
   
   )
