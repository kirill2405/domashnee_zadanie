from django.shortcuts import render
from .models import speakers
from .form import SpeakersEditForm
from .form import Add_speaker
from django.http import HttpResponseRedirect
from django.core.urlresolvers  import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def add(request):
    speakers_add_form = Add_speaker()
    if request.method == 'POST':
        speakers_add_form = Add_speaker(request.POST)
        if speakers_add_form.is_valid():
            speakers_add_form.save() 
            return HttpResponseRedirect(reverse('add'))
    else:
        speakers_add_form = Add_speakers()
    speakers_list = speakers.objects.all()
    context = {'speakers': speakers_list,'speakers_add_form':speakers_add_form}
    return render(request, 'speakers/index.html', context)


def index(request):
 speakers_list = get_queryset(request)
 context = {'speakers': speakers_list}
 return render(request, 'speakers/index.html', context)
 
def edit(request,speakers_id):
    before_edit=speakers.objects.get(id= speakers_id)
    if request.POST:
        form=SpeakersEditForm(request.POST,instance=before_edit)
        if form.is_valid():
            p = form.save()               
            return HttpResponseRedirect(reverse('speakers'))
        else:
            return render(request, 'speakers/speakers_edit.html', {'form': form})
    if speakers_id:
        form=SpeakersEditForm(instance=speakers.objects.get(id= speakers_id))
    else:
        form=SpeakersEditForm()
    return render(request,'speakers/speakers_edit.html', {'form': form, 'speakers_item': before_edit})

    
def publish(request, speakers_id):
    n = speakers.objects.get(id = speakers_id)
    n.leader = True
    n.save()
    return render(request, 'speakers/index.html', {'speakers': speakers.objects.all})

def delete(request,speakers_id):
    d = speakers.objects.get(id=speakers_id)
    d.delete()
    return HttpResponseRedirect(reverse('speakers'))

def get_queryset(request):
    speak = speakers.objects.all()
    
    paginator = Paginator(speak, 3)
    page = request.GET.get('page')
    try:
        speak = paginator.page(page)
    except PageNotAnInteger:
        speak = paginator.page(1)
    except EmptyPage:
    
        speak = paginator.page(paginator.num_pages)
    return speak


 