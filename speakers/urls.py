from django.conf.urls import patterns, url

from speakers import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='speakers'),
    url(r'^/(?P<speakers_id>\d+)/$', 'speakers.views.edit', name = 'edit'),
    url(r'^speakers/details/(?P<speakers_id>\d+)/$', 'speakers.views.publish', name = 'publish'),
    url(r'^del/(?P<speakers_id>\d+)/$', 'speakers.views.delete', name = 'delete'),
    )
