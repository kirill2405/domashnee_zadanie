from django.forms import ModelForm
from .models import speakers

class SpeakersEditForm(ModelForm):
        class Meta:
            model = speakers
            fields = ['last_name', 'first_name']
            
class Add_speaker(ModelForm):
    class Meta:
        model = speakers
        fields = ['first_name', 'last_name']

