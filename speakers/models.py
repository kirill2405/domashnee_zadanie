from django.db import models
class speakers(models.Model):
    first_name = models.CharField(max_length=70)
    last_name = models.CharField(max_length=255)
    leader = models.BooleanField(default=True)


    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)
