# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0002_auto_20150519_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='speakers',
            name='leader',
            field=models.BooleanField(default=True),
        ),
    ]
