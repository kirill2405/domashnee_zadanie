# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speakers', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='speakers',
            old_name='title',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='speakers',
            old_name='description',
            new_name='last_name',
        ),
        migrations.RemoveField(
            model_name='speakers',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='speakers',
            name='text',
        ),
    ]
