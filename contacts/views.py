from django.shortcuts import render
from .models import contacts
def index(request):
 news_list = contacts.objects.all()
 context = {'contacts': news_list}
 return render(request, 'contacts/index.html', context)
